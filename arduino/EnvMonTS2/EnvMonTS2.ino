#include <WiFiNINA.h>

#include <Ethernet.h>
#include <scpiparser.h>
#include <SPI.h>
#include <math.h>
#include <DFRobot_RGBLCD.h>
#include <Wire.h>

#define SECRET_SSID "_PeC_"
#define SECRET_PASS "HH2Lund+PC"

/*
  EnvMonTS2

*/

char ssid[] = SECRET_SSID;    // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int status = WL_IDLE_STATUS;  // the Wifi radio's status

bool debug = false;

WiFiServer WIFIserver(8082);
EthernetServer SCPIserver(5025);
DFRobot_RGBLCD lcd(16,2);
byte mac[] = {
  0xAC, 0xFD, 0xEC, 0x8D, 0x16, 0xCF
};
bool gotAMessage = false;

struct scpi_parser_context ctx;

scpi_error_t identify(struct scpi_parser_context* context, struct scpi_token* command);
scpi_error_t get_temperature(struct scpi_parser_context* context, struct scpi_token* command);
scpi_error_t get_humidity(struct scpi_parser_context* context, struct scpi_token* command);

String rstring;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Wire.begin();
  lcd.init();
  lcd.setRGB(255,255,255);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // WiFi Service
  startWiFiServer();
  // SCPI Service
  startSCPIServer();
}

// the loop routine runs over and over again forever:
void loop() {
  // Checks changes in the network
  switch (Ethernet.maintain()) {
    case 1:
      //renewed fail
      Serial.println("Error: renewed fail");
      break;
    case 2:
      //renewed success
      Serial.println("Renewed success");
      //print your local IP address:
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;
    case 3:
      //rebind fail
      Serial.println("Error: rebind fail");
      break;
    case 4:
      //rebind success
      Serial.println("Rebind success");
      //print your local IP address:
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;
    default:
      //nothing happened
      break;
  }
  
  float TempK,TempC,Hum;
  // read the input on analog pin 0:
  int A0Value = analogRead(A0);
  int A1Value = analogRead(A1);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float Tvoltage = A0Value * (5.0 / 1023.0);
  float Pvoltage = A1Value * (5.0 / 1023.0);
  // print out the value you read:
  TempK=TvalK(Tvoltage);
  TempC=TvalC(Tvoltage);
  Hum=Hval(Pvoltage,TempC);

  if (debug) printDebugLine(Tvoltage, Pvoltage, TempK, TempC, Hum);

  // outputs the data on the LCD
  printLCDLine(TempC,Hum);
  // serve the web interface
  serveWIFI(TempC,Hum); 
  // serve the ethernet connection
  serveSCPI(TempC,Hum);
  
  delay(1000);
}

void printLCDLine(float T, float H){
  lcd.setCursor(0,1);
  lcd.print("T=");
  lcd.print(T,1);
  lcd.print("C, H=");
  lcd.print(H,1);
  lcd.print("%");
}

void startWiFiServer(){
  lcd.setCursor(0,1);
  lcd.print("Connecting...");
  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }
  // check firmware version
  String fv = WiFi.firmwareVersion();
  Serial.println(fv);
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }
  listNetworks();
  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(ssid, pass);
    // wait 1 second for connection:
    delay(1000);
  }
  // you're connected now, so print out the data:
  Serial.print("You're connected to the network ");
  printCurrentNet();
  printWifiData();
  lcd.setCursor(0,0);
  IPAddress ip = WiFi.localIP();
  lcd.print(ip);
  // start the server over WiFi
  WIFIserver.begin();
}

void startSCPIServer(){
  // start the server over Ethernet
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    } else if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // no point in carrying on, so do nothing forevermore:
    while (true) {
      delay(1);
    }
  }

  struct scpi_command* env;

  /* First, initialise the parser. */
  scpi_init(&ctx);

  /*
   * After initialising the parser, we set up the command tree.  Ours is
   *
   *  *IDN?         -> identify
   *  :ENVIronment
   *    :TEMPerature?   -> get_temperature
   *    :HUMIDITY?      -> get_humidity
   */
  scpi_register_command(ctx.command_tree, SCPI_CL_SAMELEVEL, "*IDN?", 5, "*IDN?", 5, identify);
  env = scpi_register_command(ctx.command_tree, SCPI_CL_CHILD, "ENVIRONMENT", 11, "ENVI", 4, NULL);
  scpi_register_command(env, SCPI_CL_CHILD, "TEMPERATURE?", 12, "TEMP?", 5, get_temperature);
  scpi_register_command(env, SCPI_CL_CHILD, "HUMIDITY?", 9, "HUMI?", 5, get_humidity);

  Serial.print("Wired IP=");
  Serial.println(Ethernet.localIP());
  SCPIserver.begin();
}

void listNetworks() {
  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1) {
    Serial.println("Couldn't get a wifi connection");
    while (true);
  }
  // print the list of networks seen:
  Serial.print("number of available networks:");
  Serial.println(numSsid);
  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    Serial.print(thisNet);
    Serial.print(") ");
    Serial.print(WiFi.SSID(thisNet));
    Serial.print("\tSignal: ");
    Serial.print(WiFi.RSSI(thisNet));
    Serial.print(" dBm");
    Serial.print("\tEncryption: ");
    printEncryptionType(WiFi.encryptionType(thisNet));
    Serial.println("");
  }
}

void printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case ENC_TYPE_WEP:
      Serial.print("WEP");
      break;
    case ENC_TYPE_TKIP:
      Serial.print("WPA");
      break;
    case ENC_TYPE_CCMP:
      Serial.print("WPA2");
      break;
    case ENC_TYPE_NONE:
      Serial.print("None");
      break;
    case ENC_TYPE_AUTO:
      Serial.print("Auto");
      break;
    case ENC_TYPE_UNKNOWN:
    default:
      Serial.print("Unknown");
      break;
  }
}

void printMacAddress(byte mac[]) {
  for (int i = 5; i >= 0; i--) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i > 0) {
      Serial.print(":");
    }
  }
  Serial.println();
}

void printWifiData() {
  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC address: ");
  printMacAddress(mac);
}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print("BSSID: ");
  printMacAddress(bssid);
  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);
  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.print(encryption, HEX);
  Serial.print(" (");
  printEncryptionType(encryption);
  Serial.print(")");
  Serial.println();
}


void printDebugLine(float TV, float PV, float TK, float TC, float H){
    Serial.print("T=");
    Serial.print(TV);
    Serial.print(" ");
    Serial.print(PV);
    Serial.print(" ");
    Serial.print(TK,2);
    Serial.print("K, ");
    Serial.print(TC,2);
    Serial.print(" C, ");
    Serial.print(" Humidity=");
    Serial.print(H,3);
    Serial.println("%");
}

float TvalK(float v){
  float vcc=5000;
  float rb=10000.0;
  float rval;
  rval=v*1000./(vcc-v*1000.0)*rb;
  float ovrT;
  float a=8.54942e-4;
  float b=2.57305e-4;
  float c=1.65368e-7;
  ovrT=a+b*log(rval)+c*pow(log(rval),3);
  return 1.0/ovrT;
}

float TvalC(float v){
  return TvalK(v)-273.16;
}

float Hval(float v,float T){
  float rh;
  rh=-1.9206*pow(v,3)+14.37*pow(v,2)+3.421*v-12.4;
  return rh/(1+(T-23.0)*2.4e-3); 
}

void serveWIFI(float Temp, float Hum){
    // listen for incoming clients
  WiFiClient client = WIFIserver.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        //Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<TITLE>Environmental Monitor</TITLE>");
          // output the value of the data read
          client.println("<H1 style=\"font-family:verdana; font-size: 20pt; color: blue\">Environmental Info</H1>");
          client.print("<P style=\"font-family:verdana; font-size: 20pt\">T=");
          client.print(Temp,2);
          client.println(" C </P>");
          client.print("<P style=\"font-family:verdana; font-size: 20pt\">H=");
          client.print(Hum,2);
          client.println(" % </P>");
          client.println("<H3 style=\"font-family:verdana; font-size: 14pt; color: blue\">Server IP</H3>");
          client.print("<P style=\"font-family:verdana; font-size: 12pt\">Network IP=");
          client.print(Ethernet.localIP());
          client.print("</BR>WiFi IP=");
          client.print(WiFi.localIP());
          client.println("</P>");
          client.println("<H4 style=\"font-family:verdana; font-size: 12pt\">Paolo Pierini, paolo.pierini@ess.eu, 2020</H4>");
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}

void serveSCPI(float T, float H){
  // wait for a new client:
  EthernetClient client = SCPIserver.available();

  // when the client sends the first byte, say hello:
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        rstring += c;
        if (c== '\n'){
          Serial.println(rstring);
          delay(1);
          client.stop();
          rstring=""; 
        }
      }
    }
  }
    
//    if (!gotAMessage) {
//      Serial.println("We have a new client");
//      client.println("Hello, client!");
//      gotAMessage = true;
//    }
//
//    // read the bytes incoming from the client:
//    char thisChar = client.read();
//    // echo the bytes back to the client:
//    SCPIserver.write(thisChar);
//    // echo the bytes to the server as well:
//    Serial.print(thisChar);
    Ethernet.maintain();
}

/*
 * Respond to *IDN?
 */
scpi_error_t identify(struct scpi_parser_context* context, struct scpi_token* command)
{
  scpi_free_tokens(command);
  SCPIserver.println("OIC,Embedded SCPI TS2 Env Sens,1,10");
  return SCPI_SUCCESS;
}

/**
 * Read the voltage on A0.
 */
scpi_error_t get_temperature(struct scpi_parser_context* context, struct scpi_token* command)
{
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage;
  float TC;
  voltage= analogRead(A0) * (5.0f / 1024.0);
  // print out the value you read:
  TC=TvalC(voltage);
  SCPIserver.print(TC,4);
  SCPIserver.println(" C");
  scpi_free_tokens(command);
  return SCPI_SUCCESS;
}

scpi_error_t get_humidity(struct scpi_parser_context* context, struct scpi_token* command)
{
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float tvoltage;
  float hvoltage;
  float TC,H;
  tvoltage= analogRead(A0) * (5.0f / 1024.0);
  hvoltage= analogRead(A1) * (5.0f / 1024.0);
  // print out the value you read:
  TC=TvalC(tvoltage);
  H=Hval(hvoltage,TC);
  SCPIserver.print(H,4);
  SCPIserver.println(" PERC");
  scpi_free_tokens(command);
  return SCPI_SUCCESS;
}
